#!/bin/bash
apt-get update;
apt-get -y -q install unzip;
csarRoot=$(find ~ -maxdepth 1 -path "*.csar");
#unzip . $csarRoot/artifacttemplates/http%3A%2F%2Fopentosca.org%2Ftypes%2Fdeclarative/WSO2BPS320/files/wso2bps-3.2.0.zip
IFS=';' read -ra NAMES <<< "$DAs";
for i in "${NAMES[@]}"; do
	echo "KeyValue-Pair: "
    echo $i
    IFS=',' read -ra PATH <<< "$i";    
    	echo "Key: "
    	echo ${PATH[0]}
    	echo "Value: "
    	echo ${PATH[1]}
    	/usr/bin/unzip $csarRoot${PATH[1]} -d ~;
done
